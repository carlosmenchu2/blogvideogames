import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PaginaprincipalComponent } from './components/paginaprincipal/paginaprincipal.component';
import { FootercomponentComponent } from './components/footercomponent/footercomponent.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PaginaprincipalComponent,
    FootercomponentComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
